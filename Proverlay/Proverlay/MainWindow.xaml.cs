﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Interop;

namespace Proverlay
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
//        [DllImport("user32.dll", SetLastError = true)]
//        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

//        public const string WINDOW_PREFIX = "Tibi*";
//        private IntPtr handle = FindWindow(null, WINDOW_PREFIX);

        public struct RECT
        {
            public int left, top, right, bottom;
        }

        RECT rect;

//        [DllImport("user32.dll")]
//        static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

//        [DllImport("user32.dll", SetLastError = true)]
//        static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder strText, int maxCount);

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        private static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool EnumWindows(EnumWindowsProc lpEnumFunc, IntPtr lParam);

        public delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam);

        private Thread _posThread;

        public MainWindow()
        {
            Loaded += MainWindow_Loaded;
            InitializeComponent();
        }

        private void MainWindow_Exit(object sender, EventArgs e)
        {
            KeyboardHook.StopKeyboardHook();
            _posThread.Abort();
        }

        private void MainWindow_Loaded(object sender, EventArgs e)
        {
            Application.Current.MainWindow = this;

            _posThread = new Thread(UpdatePos);
            _posThread.Start();

            KeyboardHook.StartKeyboardHook();
        }

        private void SetPos()
        {
            Application.Current.MainWindow.Show();
            Application.Current.MainWindow.Height = rect.bottom - rect.top;
            Application.Current.MainWindow.Width = rect.right - rect.left;
            Application.Current.MainWindow.Top = rect.top;
            Application.Current.MainWindow.Left = rect.left;
        }

        private void UpdatePos()
        {
            while (true)
            {
                var windows = FindWindowsWithText("Tibia");

                var intPtrs = windows as IntPtr[] ?? windows.ToArray();
                if (intPtrs.Any())
                {
                    if (IsWindowVisible(intPtrs.ElementAt(0)))
                    {
                        GetWindowRect(intPtrs.ElementAt(0), out rect);

                        var uiUpdate = (Action) delegate
                        {
                            Topmost = true;
                            SetPos();
                        };
                        Dispatcher.Invoke(uiUpdate, new object[] { });
                    }
                    else
                    {
                        var hide = (Action) delegate
                        {
                            Application.Current.MainWindow.Hide();
                        };
                        Dispatcher.Invoke(hide, new object[] { });

                    }
                }
                Thread.Sleep(200);
            }
        }

        private static string GetWindowText(IntPtr hWnd)
        {
            var size = GetWindowTextLength(hWnd);
            if (size <= 0) return string.Empty;
            var builder = new StringBuilder(size + 1);
            GetWindowText(hWnd, builder, builder.Capacity);
            return builder.ToString();

        }

        /// <summary> Find all windows that match the given filter </summary>
        /// <param name="filter"> A delegate that returns true for windows
        ///    that should be returned and false for windows that should
        ///    not be returned </param>
        private static IEnumerable<IntPtr> FindWindows(EnumWindowsProc filter)
        {
            var windows = new List<IntPtr>();

            EnumWindows(delegate(IntPtr wnd, IntPtr param)
            {
                if (filter(wnd, param))
                {
                    // only add the windows that pass the filter
                    windows.Add(wnd);
                }

                // but return true here so that we iterate all windows
                return true;
            }, IntPtr.Zero);

            return windows;
        }

        /// <summary> Find all windows that contain the given title text </summary>
        /// <param name="titleText"> The text that the window title must contain. </param>
        private static IEnumerable<IntPtr> FindWindowsWithText(string titleText)
        {
            return FindWindows(delegate(IntPtr wnd, IntPtr param)
            {
                if (GetWindowText(wnd).Contains(titleText))
                {
                    System.Diagnostics.Debug.WriteLine("Found window");
                    return true;
                }

                return GetWindowText(wnd).Contains(titleText);
            });
        }

    }
}